package devcraft.parking.jpa;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.data.repository.CrudRepository;

public interface ParkingEntryRepository extends CrudRepository<ParkingEntryRepository.ParkingEntry, Long> {

    @Entity(name = "parking_entry")
    public static class ParkingEntry {

        @Id
        @GeneratedValue
        Long code;
        long time;

        public Long getCode() {
            return code;
        }

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }
    }

}