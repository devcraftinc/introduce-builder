package devcraft.parking;


import static java.time.ZoneOffset.UTC;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalTime;

public class PaymentService {

    private final ParkingStore store;
    private Clock clock;

    public interface Clock {
        long now();
    }

    public PaymentService(Clock clock, ParkingStore store) {
        this.clock = clock;
        this.store = store;
    }

    public long enterParking() {
        long entryTime = clock.now();
        return store.addParkingEntry(entryTime);
    }

    public int calcPayment(long code) {
        long entryTime = store.getParkingEntry(code);
        long paymentTime = clock.now();
        return calcPayment(entryTime, paymentTime);
    }

	private int calcPayment(long entryTime, long paymentTime) {
        PaymentPolicy WEEKDAY_PAYMENT_POLICY = new LimitedPaymentPolicy(new HourlyPaymentPolicy());
        PaymentPolicy WEEKEND_POLICY = new LimitedPaymentPolicy(new DayAndNightPaymentPolicy(
                LocalTime.of(22, 0), new HourlyPaymentPolicy(), new FixedPricePolicy(40)));

        WeeklyPaymentPolicy weeklyPaymentPolicy = new WeeklyPaymentPolicy();
        weeklyPaymentPolicy.setPaymentPolicy(DayOfWeek.SUNDAY, WEEKDAY_PAYMENT_POLICY);
        weeklyPaymentPolicy.setPaymentPolicy(DayOfWeek.MONDAY, WEEKDAY_PAYMENT_POLICY);
        weeklyPaymentPolicy.setPaymentPolicy(DayOfWeek.TUESDAY, WEEKDAY_PAYMENT_POLICY);
        weeklyPaymentPolicy.setPaymentPolicy(DayOfWeek.WEDNESDAY, WEEKDAY_PAYMENT_POLICY);
        weeklyPaymentPolicy.setPaymentPolicy(DayOfWeek.THURSDAY, WEEKEND_POLICY);
        weeklyPaymentPolicy.setPaymentPolicy(DayOfWeek.FRIDAY, WEEKEND_POLICY);
        weeklyPaymentPolicy.setPaymentPolicy(DayOfWeek.SATURDAY, WEEKEND_POLICY);
        EscapeTimePaymentPolicy escapeTimePaymentPolicy = new EscapeTimePaymentPolicy(Duration.ofMinutes(10), weeklyPaymentPolicy);
        return escapeTimePaymentPolicy.calcPayment(Instant.ofEpochMilli(entryTime), Instant.ofEpochMilli(paymentTime), UTC);
	}

}