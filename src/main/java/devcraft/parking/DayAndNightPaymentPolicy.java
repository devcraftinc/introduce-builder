package devcraft.parking;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class DayAndNightPaymentPolicy implements PaymentPolicy {

    private LocalTime nightStart;
    private PaymentPolicy dayPolicy;
    private PaymentPolicy nightPolicy;

    public DayAndNightPaymentPolicy(LocalTime nightStart, PaymentPolicy dayPolicy, PaymentPolicy nightPolicy) {
        this.nightStart = nightStart;
        this.dayPolicy = dayPolicy;
        this.nightPolicy = nightPolicy;
    }

    @Override
    public int calcPayment(Instant entryTime, Instant paymentTime, ZoneId zoneId) {
        ZonedDateTime zonedEntryTime = ZonedDateTime.ofInstant(entryTime, zoneId);
        ZonedDateTime zonedNightStart = ZonedDateTime.of(zonedEntryTime.toLocalDate(), nightStart, zoneId);

        int amountToPay = 0;
        if (entryTime.isBefore(zonedNightStart.toInstant()))
            amountToPay += dayPolicy.calcPayment(entryTime, min(paymentTime,zonedNightStart.toInstant()),zoneId);
        if (paymentTime.isAfter(zonedNightStart.toInstant()))
            amountToPay += nightPolicy.calcPayment(max(entryTime,zonedNightStart.toInstant()),paymentTime,zoneId);
        
        return amountToPay;
    }

    private Instant max(Instant instant1, Instant instant2) {
        return Instant.ofEpochMilli(Math.max(instant1.toEpochMilli(), instant2.toEpochMilli()));
    }

    private Instant min(Instant instant1, Instant instant2) {
        return Instant.ofEpochMilli(Math.min(instant1.toEpochMilli(), instant2.toEpochMilli()));
    }
}
