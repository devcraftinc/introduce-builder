package devcraft.parking;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;

class HourlyPaymentPolicy implements PaymentPolicy {
	private Duration firstPaymentInterval = Duration.ofMinutes(60);
	private int firstIntervalRate = 12;
	private long paymentInterval = minAsMillis(15);
	private int intervalRate = 3;

	public int calcPayment(Instant entryTime, Instant paymentTime, ZoneId zoneId) {
		Duration timeInParking = Duration.between(entryTime, paymentTime);
		if (timeInParking.toMillis() < firstPaymentInterval.toMillis()) {
			return this.firstIntervalRate;
		}
		long intervalsToPay = calcIntervalsToPay(timeInParking.toMillis());
		return (int) (this.firstIntervalRate + (intervalsToPay * intervalRate));
	}

	private long calcIntervalsToPay(long timeInParking) {
		return 1 + (timeInParking - firstPaymentInterval.toMillis()) / paymentInterval;
	}

	private static long minAsMillis(int min) {
		return min * 60 * 1000L;
	}
}