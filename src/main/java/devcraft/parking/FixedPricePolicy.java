package devcraft.parking;

import java.time.Instant;
import java.time.ZoneId;

public class FixedPricePolicy implements PaymentPolicy {

    private final int price;

    public FixedPricePolicy(int price) {
        this.price = price;
    }

    @Override
    public int calcPayment(Instant entryTime, Instant paymentTime, ZoneId zoneId) {
        return price;
    }
}
