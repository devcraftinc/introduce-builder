package devcraft.parking;

public interface ParkingStore {
    long addParkingEntry(long entryTime);

    long getParkingEntry(long code);
}
