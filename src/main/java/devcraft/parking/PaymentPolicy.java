package devcraft.parking;

import java.time.Instant;
import java.time.ZoneId;

public interface PaymentPolicy {
    int calcPayment(Instant entryTime, Instant paymentTime, ZoneId zoneId);
}
